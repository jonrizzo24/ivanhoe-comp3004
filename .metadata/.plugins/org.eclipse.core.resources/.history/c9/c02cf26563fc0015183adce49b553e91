import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestActionCards {
Game game;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestActionCards");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestActionCards");
		game = new Game();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestActionCards");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestActionCards");
    }

	@Test
	public void testDodgeUnshielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card dodge = new Card("dodge", "dodge", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player.getHand().add(dodge);
		player.getThread().setSending("ACARD&" + dodge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("DODGE&" + dodge.getFile() + "\n" + 
				player2.getName() +"\n" + player2.getDisplay().get(1).getFile());
		game.turn("purple", player);
		
		assertEquals(1, player2.getDisplay().size());
		assertEquals(4, player2.getDisplay().get(0).getValue());
		assertEquals(2, player.getDisplay().size());
	}
	@Test
	public void testDodgeShielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card dodge = new Card("dodge", "dodge", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(dodge);
		player.getThread().setSending("ACARD&" + dodge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("DODGE&" + dodge.getFile() + "\n" + 
				player2.getName() +"\n" + player2.getDisplay().get(1).getFile());
		game.turn("purple", player);
		
		assertEquals(2, player2.getDisplay().size());
		assertEquals(4, player2.getDisplay().get(0).getValue());
		assertEquals(7, player2.getDisplay().get(1).getValue());
		assertEquals(2, player.getDisplay().size());
	}
	@Test
	public void testDisgraceUnshielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card disgrace = new Card("disgrace", "disgrace", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player.getHand().add(disgrace);
		player.getThread().setSending("ACARD&" + disgrace.getFile());
		game.turn("purple", player);
		player.getThread().setSending("DISGRACE&" + disgrace.getFile());
		game.turn("purple", player);
		
		assertEquals(2, player2.getDisplay().size());
		assertEquals(4, player2.getDisplay().get(0).getValue());
		assertEquals(2, player.getDisplay().size());
	}
	@Test
	public void testDisgraceShielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card disgrace = new Card("disgrace", "disgrace", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(disgrace);
		player.getThread().setSending("ACARD&" + disgrace.getFile());
		game.turn("purple", player);
		player.getThread().setSending("DISGRACE&" + disgrace.getFile());
		game.turn("purple", player);
		
		assertEquals(4, player2.getDisplay().size());
		assertEquals(4, player2.getDisplay().get(0).getValue());
		assertEquals(4, player2.getDisplay().get(1).getValue());
		assertEquals(2, player.getDisplay().size());
	}
	@Test
	public void testRetreateUnshielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card retreat = new Card("retreat", "retreat", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player.getHand().add(retreat);
		player.getThread().setSending("ACARD&" + retreat.getFile());
		game.turn("purple", player);
		player.getThread().setSending("RETREAT&\n" + player.getDisplay().get(1).getFile());
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(1, player.getHand().size());
	}
	@Test
	public void testRetreatShielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card retreat = new Card("retreat", "retreat", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(retreat);
		player.getThread().setSending("ACARD&" + retreat.getFile());
		game.turn("purple", player);
		player.getThread().setSending("RETREAT&\n" + player.getDisplay().get(1).getFile());
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(1, player.getHand().size());
	}

	@Test
	public void testRiposteUnshielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card riposte = new Card("riposte", "riposte", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player.getHand().add(riposte);
		player.getThread().setSending("ACARD&" + riposte.getFile());
		game.turn("purple", player);
		player.getThread().setSending("RIPOSTE&\n" + player2.getName());
		game.turn("purple", player);
		
		assertEquals(5, player.getDisplay().size());
		assertEquals(3, player2.getDisplay().size());
	}
	@Test
	public void testRiposteShielded() throws IOException{
		Player player;
		Player player2;
		for(int i = 0; i < 2; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card riposte = new Card("riposte", "riposte", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(riposte);
		player.getThread().setSending("ACARD&" + riposte.getFile());
		game.turn("purple", player);
		player.getThread().setSending("RIPOSTE&\n" + player2.getName());
		game.turn("purple", player);
		
		assertEquals(4, player.getDisplay().size());
		assertEquals(4, player2.getDisplay().size());
	}

	@Test
	public void testOutmanouverUnshielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card outmanouver = new Card("outmanouver", "outmanouver", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player.getHand().add(outmanouver);
		player.getThread().setSending("ACARD&" + outmanouver.getFile());
		game.turn("purple", player);
		player.getThread().setSending("OUTMANOUVER&\n");
		game.turn("purple", player);
		
		assertEquals(4, player.getDisplay().size());
		assertEquals(3, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
	}
	@Test
	public void testOutmanouverShielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card outmanouver = new Card("outmanouver", "outmanouver", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(outmanouver);
		player.getThread().setSending("ACARD&" + outmanouver.getFile());
		game.turn("purple", player);
		player.getThread().setSending("OUTMANOUVER&\n" );
		game.turn("purple", player);
		
		assertEquals(4, player.getDisplay().size());
		assertEquals(4, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
	}
	@Test
	public void testCounterChargeUnshielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card countercharge = new Card("counter-charge", "counter-charge", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player.getHand().add(countercharge);
		player.getThread().setSending("ACARD&" + countercharge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("CNTRCHRG&\n");
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(3, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(2, player3.getDisplay().get(2).getValue());
		
	}
	@Test
	public void testCounterChargeShielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card countercharge = new Card("counter-charge", "counter-charge", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(countercharge);
		player.getThread().setSending("ACARD&" + countercharge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("CNTRCHRG&\n" );
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(4, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(2, player3.getDisplay().get(2).getValue());
	}

	@Test
	public void testChargeUnshielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card charge = new Card("charge", "charge", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player.getHand().add(charge);
		player.getThread().setSending("ACARD&" + charge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("CHARGE&\n");
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(3, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(7, player3.getDisplay().get(2).getValue());
	}
	@Test
	public void testChargeShielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card charge = new Card("charge", "charge", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(charge);
		player.getThread().setSending("ACARD&" + charge.getFile());
		game.turn("purple", player);
		player.getThread().setSending("CHARGE&\n" );
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(4, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(7, player3.getDisplay().get(2).getValue());
	}
	
	@Test
	public void testBreakLanceUnshielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card breakLance = new Card("breakLance", "breakLance", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player.getHand().add(breakLance);
		player.getThread().setSending("ACARD&" + breakLance.getFile());
		game.turn("purple", player);
		player.getThread().setSending("BREAKLANCE&\n" + p2.getName());
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(3, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(7, player3.getDisplay().get(2).getValue());
	}
	@Test
	public void testBreakLanceShielded() throws IOException{
		Player player;
		Player player2;
		Player player3;
		for(int i = 0; i < 3; i++){
			final Socket socket = mock(Socket.class);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			ByteArrayInputStream in = new ByteArrayInputStream(("Player" + i +  "\n").getBytes());
			when(socket.getInputStream()).thenReturn(in);
			when(socket.getOutputStream()).thenReturn(out);
			ServerThread s = new ServerThread(socket);
			s.open();
			Player play = new Player(Integer.toString(i), s, i);
			game.addPlayer(play);
			play.getDisplay().add(new Card("axe", "purple4", "purple", 4));
			play.getDisplay().add(new Card("maiden", "maiden6", "supporter", 4));
			play.getDisplay().add(new Card("squire", "squire2", "supporter", 2));
			play.getDisplay().add(new Card("axe", "purple7", "purple", 7));
		}
		Card breakLance = new Card("breakLance", "breakLance", "action", 0);
		Card shield = new Card("shield", "shield", "action", 0);
		player = game.getPlayers().get(0);
		player2 = game.getPlayers().get(1);
		player3 = game.getPlayers().get(2);
		player2.setShield(true);
		player2.getSpecials().add(shield);
		player.getHand().add(breakLance);
		player.getThread().setSending("ACARD&" + breakLance.getFile());
		game.turn("purple", player);
		player.getThread().setSending("BREAKLANCE&\n" + player2.getName());
		game.turn("purple", player);
		
		assertEquals(3, player.getDisplay().size());
		assertEquals(4, player2.getDisplay().size());
		assertEquals(3, player3.getDisplay().size());
		assertEquals(7, player3.getDisplay().get(2).getValue());
	}
	

}
