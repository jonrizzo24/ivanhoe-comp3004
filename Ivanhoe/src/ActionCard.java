
public class ActionCard extends Card {
		//card has name and file 
		private String action;

		public ActionCard(String n, String f, String a){
			super(n,f, null, 0);
			action = a;
			
		}
		
		public String getAction(){
			return action;
		}
}
