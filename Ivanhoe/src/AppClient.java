import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class AppClient implements Runnable{
	
	private int ID = 0;
	private Socket socket            = null;
	private Thread thread            = null;
	private ClientThread   client    = null;
	private BufferedReader console   = null;
	private BufferedReader streamIn  = null;
	private BufferedWriter streamOut = null;
	
	private UImanager ui;
	
	public AppClient (String serverName, int serverPort) {  
		System.out.println(ID + ": Establishing connection. Please wait ...");
		ui = new UImanager(this);
		try {  
			this.socket = new Socket(serverName, serverPort);
			this.ID = socket.getLocalPort();
	    	System.out.println(ID + ": Connected to server: " + socket.getInetAddress());
	    	System.out.println(ID + ": Connected to portid: " + socket.getLocalPort());
	      this.start();
		} catch(UnknownHostException uhe) {  
			System.err.println(ID + ": Unknown Host");
			Trace.getInstance().exception(this,uhe);
		} catch(IOException ioe) {  
			System.out.println(ID + ": Unexpected exception");
			Trace.getInstance().exception(this,ioe);
	   }
	}
	
	public int getID () {
		return this.ID;
	}
	
   public void start() throws IOException {  
	   try {
		   console	= new BufferedReader(new InputStreamReader(System.in));
		   streamIn	= new BufferedReader(new InputStreamReader(socket.getInputStream()));
		   streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

		   if (thread == null) {  
		      client = new ClientThread(this, socket);
		      thread = new Thread(this);                   
		      thread.start();
		   }
		   //ui = new UImanager();
		   ui.setVisible(true); 
	   } catch (IOException ioe) {
      	Trace.getInstance().exception(this,ioe);
         throw ioe;
	   }
   }
   
   @Override
public void run() { 
		System.out.println(ID + ": Client Started...");
		while (thread != null) {  
			try {  
				if (streamOut != null) {
					streamOut.flush();
					streamOut.write(console.readLine() + "\n");
				} else {
					System.out.println(ID + ": Stream Closed");
				}
        }
        catch(IOException e) {  
        	Trace.getInstance().exception(this,e);
        	stop();
        }}
		System.out.println(ID + ": Client Stopped...");
  }
   
   public synchronized void handle (String msg) {
	   System.out.println(msg);
	   
	   String[] msgParse = parseMessage(msg);
	   if(parseMessage(msg)[0].equals("Enter Name")){
		   
		   int players; 
		   players = Integer.parseInt(parseMessage(msg)[1]);
		   
		   
		   ui.setPlayerDisplays(players);
		   String name = ui.promptName();
		   System.out.println(name);
		   try {
			streamOut.write(name + "\n");
			streamOut.flush();
		} catch (IOException e) {
			Trace.getInstance().exception(this,e);
			e.printStackTrace();
		}
	   }
	   
	   else if(parseMessage(msg)[0].equals("Names")){
		   ArrayList<String> names = new ArrayList<String>(Arrays.asList(parseMessage(msg.substring(6))));
		   ui.addNames(names);
	   }
	   
	   else if(msgParse[0].equals("MSG"))
		   ui.showMessage(msgParse[1]);
	   
	   else if(msgParse[0].equals("CARD"))
		   ui.addCardToHand(msgParse[1]);
	   
	  else if(msgParse[0].equals("CLR")){
		   ui.showMessage(msgParse[2]);
		   ui.selectColor();
	  }
	  else if(msgParse[0].equals("TCLR"))
		  ui.setTourneyColor(msgParse[1]);
	  
	   
	   else if(msgParse[0].equals("SEND")){
		   try {
			streamOut.write(msgParse[1]);
			streamOut.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	   }
	   else if(msgParse[0].equals("TURN")){
		   ui.showMessage(msgParse[2]);
		   ui.addCardToHand(msgParse[1]);
		   ui.makeTurn();
	   }
	   else if(msgParse[0].equals("SCR")){
		   ui.updateScore(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("OPCARD")){
		   ui.oponentCard(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("WTH")){
		   ui.withdrawed(msgParse[1]);   
	   }
	   else if(msgParse[0].equals("RWD")){
		   ui.giveToken(msgParse[1]);
	   }
	   else  if(msgParse[0].equals("OPRWD")){
		   ui.oponentToken(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("RWDP")){
		   ui.chooseToken();
	   }
	   else if(msgParse[0].equals("RST")){
		   ui.restartDisplay();
	   }
	   else if(msgParse[0].equals("END")){
		   ui.endGame();
		   System.exit(0);
	   }
	   else if(msgParse[0].equals("RTKN")){
		   ui.chooseRemoveToken();
	   }
	   else if (msgParse[0].equals("UNHORSE")){
		   ui.unhorse();
	   }
	   else if (msgParse[0].equals("CHANGEW")){
		   ui.changeWeapon();
	   }
	   else if (msgParse[0].equals("CARDRST")){
		   ui.makeTurn();
	   }
	   else if (msgParse[0].equals("RSTSCR")){
		   ui.score();
	   }
	   else if (msgParse[0].equals("RMVLST")){
		   ui.removeLastPlayed();
	   }
	   else if (msgParse[0].equals("OPRMVLST")){
		   ui.OponentRemoveLastPlayed(msgParse[1]);
	   }
	   else if (msgParse[0].equals("RMVVLU")){
		   ui.removeValue(Integer.parseInt(msgParse[1]));
	   }
	   else if(msgParse[0].equals("BREAKLANCE")){	  
		   ui.choosePlayer();	   
	   }
	   else if(msgParse[0].equals("DELPURPLE")){
		   	ui.delPurple();
	   }
	   else if(msgParse[0].equals("DELPURPLE2")){
		   
		   ui.delPurple(msgParse[1]);
		   System.out.print("sent "+msgParse[1]);
	   }
	   else if (msgParse[0].equals("RMVVLU")){
		   ui.removeValue(Integer.parseInt(msgParse[1]));
	   }
	   else if (msgParse[0].equals("OPRMVVLU")){
		   ui.oponentRemoveValue(Integer.parseInt(msgParse[1]), msgParse[2]);
	   }
	   else if(msgParse[0].equals("RIPOSTE")){	  
		   ui.choosePlayer();	   
	   }
	   else if(msgParse[0].equals("GIVECRD")){
		   ui.giveLastCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("GETCRD")){
		   ui.getLastCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("OPRPST")){
		   ui.opRiposte(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("DISGRACE")){
		   ui.disgrace();
	   }
	   else if(msgParse[0].equals("DODGE")){
		   ui.choosePlayer();
	   }
	   else if(msgParse[0].equals("CARDPICK")){
		   ui.pickCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("DELCARD")){
		   ui.delCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("DELCARD2")){
		   ui.delCard(msgParse[1],msgParse[2]);
	   }
	   else if(msgParse[0].equals("LOCALCARDPICK")){
		   ui.pickCardLocal();
	   }
	   else if(msgParse[0].equals("MOVECARD")){
		   ui.moveCardToHand(msgParse[1]);
	   }
	   else if(msgParse[0].equals("MOVECARD2")){
		   ui.moveCardToHand(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("KNOCKDOWN")){
		   ui.choosePlayer();
	   }
	   else if(msgParse[0].equals("ADDCARDTOHAND")){
		   ui.addCardToHand(msgParse[1]);
	   }
	   else if(msgParse[0].equals("REMCARDFROMHAND")){
		   ui.removeRandCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("SPOPCARD")){
		   ui.specialOponentCard(msgParse[1], msgParse[2]);
	   }
	   else if(msgParse[0].equals("SHIELD")){
		   ui.shield(msgParse[1]);
	   } 
	   else if(msgParse[0].equals("STUNNED")){
		   ui.choosePlayer();
	   }
	   else if(msgParse[0].equals("ADDSTUN")){
		   ui.addStun(msgParse[1]);
	   }
	   else if (msgParse[0].equals("ADDSTUN2")){
		   ui.addStun(msgParse[1],msgParse[2]);
	   }
	   else if(msgParse[0].equals("STUNTRN")){
		   ui.stunTurn();
	   }
	   else if(msgParse[0].equals("OWADDCARD")){
		   ui.owAddCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("OWDELCARD")){
		   ui.owDelCard(msgParse[1]);
	   }
	   else if(msgParse[0].equals("OWADDCARD2")){
		   ui.owAddCard(msgParse[1],msgParse[2]);
	   }
	   else if(msgParse[0].equals("OWDELCARD2")){
		   ui.owDelCard(msgParse[1],msgParse[2]);
	   }
	   else if(msgParse[0].equals("OWOP")){
		   ui.owOP(msgParse[1],msgParse[2],msgParse[3],msgParse[4]);
	   }
	   else if(msgParse[0].equals("AOPCARD")){
		   ui.addToDiscardPanel(msgParse[2]);
	   }
	 
	   
   }
   
   public String[] parseMessage(String s){
	   String[] tokens = s.split("~");
	   return tokens;
   }
   
   public void stop() {  
	      try { 
	      	if (thread != null) thread = null;
	    	  	if (console != null) console.close();
	    	  	if (streamIn != null) streamIn.close();
	    	  	if (streamOut != null) streamOut.close();

	    	  	if (socket != null) socket.close();

	    	  	this.socket = null;
	    	  	this.console = null;
	    	  	this.streamIn = null;
	    	  	this.streamOut = null;    	  
	      } catch(IOException ioe) {  
	    	  Trace.getInstance().exception(this,ioe);
	      }
	      client.close();  
	   }

}
