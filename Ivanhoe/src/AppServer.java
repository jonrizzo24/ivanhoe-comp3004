import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class AppServer implements Runnable{
	int clientCount = 0;
	private Filter filter;
	private Thread thread = null;
	private ServerSocket server = null;
	//private HashMap<Integer, ServerThread> clients;
	private Logger logger = Trace.getInstance().getLogger(this);
	private Game game;
	
	public AppServer(int port){
		try{

			System.out.println("Binding to port " + port + ", please wait  ...");
			Trace.getInstance().write(this, "Binding to port " + port);
			logger.info("Binding to port " + port);
			
			//clients = new HashMap<Integer, ServerThread>();
			server = new ServerSocket(port);
			server.setReuseAddress(true);
			start();
		}
		catch(IOException ioe){
			Trace.getInstance().exception(ioe);
			logger.fatal(ioe);
		}
		
	}
	
	public void start(){
		if (thread == null) {
			thread = new Thread(this);
			Trace.getInstance().write(this, "Server started: " + server, thread.getId());
			logger.info(String.format("Server started: %s %d", server,thread.getId()));
			thread.start();
		}
	}
	
	@Override
	public void run(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to the Ivanhoe Server! Press Enter to continue.");
		System.out.println("How many players will there be?");
		int p = 0;
		
		while (p < 2 || p > 5){
			p = sc.nextInt();
		}
		game = new Game(p);
		
		System.out.println("Starting game with " + p + " players!");
		for (int i = 0; i < p; i++) {
			try {
				Trace.getInstance().write(this, "Waiting for a client ...");
				logger.info("Waiting for a client ...");
				addThread(server.accept());
			} 
			catch (IOException e) {				
				Trace.getInstance().exception(this,e);
			}
		}
		System.out.println("ready to start the game!");
		game.startGame();
		sc.close();
		shutdown();
	}
	
	public void addThread(Socket socket){
		Trace.getInstance().write(this, "Client accepted", socket);
		if (clientCount < Config.MAX_CLIENTS) {
			try {
				//create a separate thread for each client
				ServerThread serverThread = new ServerThread(this, socket);
				//starts the thread
				System.out.println("opening");
				serverThread.open();
				serverThread.start();
				System.out.println("opened");
				
				String name = "Player";
				
				
				
				Player play = new Player(name, serverThread, serverThread.getID());
				
				
				//add player to list
				System.out.println("adding player to game object");
				game.addPlayer(play);
				this.clientCount++;
			} catch (IOException e) {
				Trace.getInstance().exception(this,e);
			}
		} else {
			logger.info(String.format("Client Tried to connect: %s", socket));
			logger.info(String.format("Client refused: maximum number of clients reached: d", Config.MAX_CLIENTS));

			Trace.getInstance().write(this, "Client Tried to connect", socket );
			Trace.getInstance().write(this, "Client refused: maximum number of clients reached", Config.MAX_CLIENTS );
		}
		
	}
	
	public void shutdown() {
		
		System.out.println("shutting down server..");

		if (thread != null) {
			thread = null;
		}

		try {
			for (Player p : game.getPlayers()) {
				p.getThread().close();
			}
			game.getPlayers().clear();
			server.close();
		} catch (IOException e) {
			Trace.getInstance().exception(this,e);
		}
		logger.info(String.format("Server Shutdown cleanly %s", server));
		Trace.getInstance().write(this, "Server Shutdown cleanly", server );
		Trace.getInstance().close();
		System.exit(0);
	}

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}
		
}
