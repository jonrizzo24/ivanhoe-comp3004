
public class Card {
	protected String name;
	protected String file;
	protected String color;
	protected int value;

	public Card(){
		color = null;
		name = "no name input";
		file = "/cards/";	
	}
	
	public Card(String n, String f, String c, int v){
		name = n;
		file = "/cards/"+ f +".jpg"; // should only be the file name	
		color = c;
		value = v;
	}

	public String getName(){
		return name;
	}
	public String getFile(){
		return file;
	}
	
	public String getColor(){
		return color;
	}
	
	public int getValue(){
		return value;
	}
}
