public class Config {
	public static final int MAX_CLIENTS = 5;
	public static final int MIN_CLIENTS = 2;
	public static final int DEFAULT_PORT = 5050;
	public static String DEFAULT_HOST = "192.168.0.16";
	public static final boolean PRINT_STACK_TRACE = false;
}


