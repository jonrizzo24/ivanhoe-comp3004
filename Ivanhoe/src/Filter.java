

import org.apache.log4j.Logger;

public class Filter {

	private String[] filters =  { "dog", "cat", "horse" };
	
	private Logger logger = Trace.getInstance().getLogger(this);
	
	public Boolean filter (ServerThread client, String m) {
		for (String filter : filters) {
			if (m.toLowerCase().contains(filter)) {				
				logger.info(String.format("%s:%d Message had to be filtered: it contained the word %s", client.getSocketAddress(), client.getID(), filter));
				logger.info(String.format("%s:%d : %s" ,client.getSocketAddress(), client.getID(), m));
				return Boolean.FALSE;
			}}
		return Boolean.TRUE;
	}
}
