import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class OponentPanel extends JPanel{
	
	private String name;
	private int score;
	//tokens
	private JLabel red;
	private JLabel blue;
	private JLabel green;
	private JLabel yellow;
	private JLabel purple;
	private boolean shield;
	
	private JLabel scoreLabel;
	private JLabel nameLabel;
	
	private JPanel cardDisplay;
	
	private int pos;
	private int specPos;
	
	private ArrayList<JLabel> cards;
	private ArrayList<JLabel> specials;
	
	public OponentPanel(){

		super();
		specials = new ArrayList<JLabel>();
		cards = new ArrayList<JLabel>();
		pos = 5;
		name = "Loading...";
		score = 0;
		
		setLayout(null);
		setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		setBackground(Color.WHITE);
		setBounds(new Rectangle(701, 114));
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBounds(596, 23, 99, 85);
		add(panel_8);
		
		red = new JLabel("red");
		red.setForeground(Color.RED);
		red.setBounds(49, 50, 44, 16);
		red.setVisible(false);
		panel_8.add(red);
		
		green = new JLabel("green");
		green.setForeground(Color.GREEN);
		green.setBounds(0, 31, 44, 16);
		green.setVisible(false);
		panel_8.add(green);
		
		blue = new JLabel("blue");
		blue.setForeground(Color.BLUE);
		blue.setBounds(0, 50, 27, 16);
		blue.setVisible(false);
		panel_8.add(blue);
		
		yellow = new JLabel("yellow");
		yellow.setForeground(Color.YELLOW);
		yellow.setBounds(49, 67, 44, 16);
		yellow.setVisible(false);
		panel_8.add(yellow);
		
		purple = new JLabel("purple");
		purple.setForeground(Color.MAGENTA);
		purple.setBounds(0, 67, 40, 16);
		purple.setVisible(false);
		panel_8.add(purple);
		
		JLabel label_39 = new JLabel("Tokens");
		label_39.setBounds(18, 6, 61, 16);
		panel_8.add(label_39);
		
		cardDisplay = new JPanel();
		cardDisplay.setBackground(Color.GREEN);
		cardDisplay.setBounds(6, 23, 589, 85);
		add(cardDisplay);
		cardDisplay.setLayout(null);
		
		specPos = cardDisplay.getWidth() - 87;
		
		JLabel label_12 = new JLabel("Player Name:");
		label_12.setBounds(6, 6, 89, 16);
		add(label_12);
		
		nameLabel = new JLabel(name);
		nameLabel.setBounds(88, 6, 149, 16);
		add(nameLabel);
		
		JLabel label_14 = new JLabel("Player Score: ");
		label_14.setBounds(301, 6, 83, 16);
		add(label_14);
		
		scoreLabel = new JLabel("" +score);
		scoreLabel.setBounds(382, 6, 89, 16);
		add(scoreLabel);
		
		shield = false;
	}
	
	
	@Override
	public void setName(String n){
		name = n;
		nameLabel.setText(n);
	}
	
	@Override
	public String getName(){
		return name;
	}
	
	public void setScore(int i){
		score = i;
		scoreLabel.setText(Integer.toString(i));
	}
	
	public int getScore(){
		return score;
	}
	public boolean isShieled(){
		return shield;
	}
	public void setShield(boolean y){
		shield = y;
	}
	
	public JPanel getCardDisplay(){
		return cardDisplay;
	}
	
	public int getPos(){
		return pos;
	}
	public void setPos(int i){
		pos = i;
	}
	
	public ArrayList<JLabel> getCards(){
		return cards;
	}
	public ArrayList<JLabel> getSpecials(){
		return specials;
	}
	
	public JLabel getRed(){
		return red;
	}
	public JLabel getBlue(){
		return blue;
	}
	public JLabel getGreen(){
		return green;
	}
	public JLabel getYellow(){
		return yellow;
	}
	public JLabel getPurple(){
		return purple;
	}
	
	public JLabel getNameLbl(){
		return nameLabel;
	}
	
	public int getSpecPos(){
		return specPos;
	}
	public void setSpecPos(int p){
		specPos = p;
	}

}
