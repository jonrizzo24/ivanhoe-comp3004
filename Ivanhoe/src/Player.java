import java.util.ArrayList;

public class Player {
	private String name;
	private ServerThread thread;
	private int id;
	private ArrayList<Card> hand;
	private int score;
	private boolean withdrawn = false;
	private ArrayList<Card> display;
	
	private ArrayList<String> tokens;
	private ArrayList<Card> specials;
	
	private boolean shield;
	private boolean stun;
	
	public Player(String n, ServerThread t, int i){
		name = n;
		thread = t;
		id = i;
		hand = new ArrayList<Card>();
		score = 0;
		display = new ArrayList<Card>();
		tokens = new ArrayList<String>();
		specials = new ArrayList<Card>();
		shield = false;
		stun = false;
	}
	
	public Player(){ //constructor for test cases
		name = "Player";
		thread = null;
		id = 0;
		hand = new ArrayList<Card>();
		tokens = new ArrayList<String>();
	}
	
	public ArrayList<Card> getSpecials(){
		return specials;
	}
	
	public ServerThread getThread(){
		return thread;
	}
	
	public int getID(){
		return id;
	}
	public boolean isShieled(){
		return shield;
	}
	public void setShield(boolean y){
		shield = y;
	}
	public boolean isStunned(){
		return stun;
	}
	public void setStun(boolean b){
		stun = b;
	}
	
	@Override
	public String toString(){
		return "Player : " + name + " Thread: " + thread.getID() + "\n";
	}
	public void setName(String n){
		name = n;
	}
	
	public String getName(){
		return name;
	}
	public void addToHand(Card c){
		hand.add(c);
	}
	public int getHandSize(){
		return hand.size();
	}
	public ArrayList<Card> getHand(){
		return hand;
	}
	
	public void setScore(int s){
		score = s;
	}
	public int calcScore(){
		int s =0;
		for (Card c:display){ 
			s+= c.getValue();
		}
		score = s;
		return s;
	}
	
	public int getScore(){
		return score;
	}
	public void setWithdraw(boolean b){
		withdrawn = b;
	}
	public boolean getWithdraw(){
		return withdrawn;
	}
	public ArrayList<Card> getDisplay(){
		return display;
	}
	public ArrayList<String> getTokens(){
		return tokens;
	}
	
	
}
