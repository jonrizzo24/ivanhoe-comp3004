
public class Token {
	private String colour;
	
	public Token(String c){
		colour = c;
	}
	
	public String getColour(){
		return colour;
	}
}
