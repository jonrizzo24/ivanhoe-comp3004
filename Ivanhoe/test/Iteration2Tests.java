import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestActionCards.class, TestCreateDeck.class, TestFindChampion.class, TestMaiden.class,
		TestOneCardInDisplay.class, TestPurpleChooseToken.class, TestTournament.class })
public class Iteration2Tests {

}
