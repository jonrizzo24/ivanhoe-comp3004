import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;

public class TestDealCards {
	
	Game game;
	
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGameStart");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGameStart");
		game = new Game();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGameStart");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGameStart");
    }
    @Test
	public void testDealCards2() {
		for(int i =0;i<2;i++){
			game.addPlayer(new Player());
		}
		game.createDeck();
		game.dealCards();
		for(int i =0;i<game.getPlayers().size();i++){
			assertEquals(8,game.getPlayers().get(i).getHandSize());	
		}
		
	}
    @Test
	public void testDealCards3() {
		for(int i =0;i<3;i++){
			game.addPlayer(new Player());
		}
		game.createDeck();
		game.dealCards();
		for(int i =0;i<game.getPlayers().size();i++){
			assertEquals(8,game.getPlayers().get(i).getHandSize());	
		}
		
	}
    @Test
	public void testDealCards4() {
		for(int i =0;i<4;i++){
			game.addPlayer(new Player());
		}
		game.createDeck();
		game.dealCards();
		for(int i =0;i<game.getPlayers().size();i++){
			assertEquals(8,game.getPlayers().get(i).getHandSize());	
		}
		
	}
    @Test
	public void testDealCards5() {
		for(int i =0;i<5;i++){
			game.addPlayer(new Player());
		}
		game.createDeck();
		game.dealCards();
		for(int i =0;i<game.getPlayers().size();i++){
			assertEquals(8,game.getPlayers().get(i).getHandSize());	
		}
		
	}

}
