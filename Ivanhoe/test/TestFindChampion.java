import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

	import static org.junit.Assert.*;

	import org.junit.After;
	import org.junit.AfterClass;
	import org.junit.Before;
	import org.junit.BeforeClass;
	import org.junit.Test;
	
	
	public class TestFindChampion {
		
	static Game game;
	static ArrayList<String> temp;	
		
		@BeforeClass
	    public static void BeforeClass() {
	        System.out.println("@BeforeClass: TestFindChampion");
	        
	    }

	    /** This is processed/initialized before each test is conducted */
	    @Before
	    public void setUp() {
			System.out.println("@Before(): TestFindChampion");
			
			
		}
		
	    /** This is processed/initialized after each test is conducted */
	    @After
	    public void tearDown () {
			System.out.println("@After(): TestFindChampion");
	
		}
		
	    /** This will be processed after the Test Class has been destroyed */
	    @AfterClass
	    public static void afterClass () {
	    	 System.out.println("@AfterClass: TestFindChampion");
	    }
	    
	    
	    
	    
	    @Test
	    public void findChamp2(){
	    	game = new Game(2); // new game with expected players
	    	game.setObjective(2); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	temp.add("green");
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(true,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp3(){
	    	game = new Game(3); // new game with expected players
	    	game.setObjective(3); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	temp.add("green");
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(true,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp4(){
	    	game = new Game(4); // new game with expected players
	    	game.setObjective(4); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	//only need 4 tokens to win
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(true,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp5(){
	    	game = new Game(5); // new game with expected players
	    	game.setObjective(5); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	//only need 4 tokens to win
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(true,game.findChamp()); // find winner
	    }
	    
	    @Test
	    public void findChamp2fail(){
	    	game = new Game(2); // new game with expected players
	    	game.setObjective(2); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	//temp.add("green");
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(false,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp3fail(){
	    	game = new Game(3); // new game with expected players
	    	game.setObjective(3); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	temp.add("green");
	    	//temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(false,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp4fail(){
	    	game = new Game(4); // new game with expected players
	    	game.setObjective(4); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	//only need 4 tokens to win
	    	temp.add("red");
	    	temp.add("yellow");
	    	temp.add("blue");
	    	//temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(false,game.findChamp()); // find winner
	    }
	    @Test
	    public void findChamp5fail(){
	    	game = new Game(5); // new game with expected players
	    	game.setObjective(5); // set objectives
	    	temp = new ArrayList<String>(); // hold all toekens
	    	//only need 4 tokens to win
	    	temp.add("red");
	    	temp.add("yellow");
	    	//temp.add("blue");
	    	temp.add("purple");
	    	game.addPlayer(new Player()); //add players
	    	game.setLastWinner(new Player()); //add winner
	    	game.lastWinner.getTokens().addAll(temp); // give winner tokens
	    	assertEquals(false,game.findChamp()); // find winner
	    }
	}
	
