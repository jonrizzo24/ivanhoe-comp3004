import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestFindWinner {
	
Game game;
	
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestFindWinner");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestFindWinner");
		game = new Game();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestFindWinner");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestFindWinner");
    }

	@Test
	public void testFindWinner1() {
		//add two players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first one to withdrawn
		game.getPlayers().get(0).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(1), winner);
	}
	
	@Test
	public void testFindWinner2() {
		//add two players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set second one to withdrawn
		game.getPlayers().get(1).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(0), winner);
	}
	
	@Test
	public void testFindWinner3() {
		//add two players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set none to withdrawn
		
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(false, winnerExists);
	}
	
	@Test
	public void testFindWinner4() {
		//add 3 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first, second one to withdrawn
		game.getPlayers().get(1).setWithdraw(true);
		game.getPlayers().get(0).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(2), winner);
	}
	
	@Test
	public void testFindWinner5() {
		//add 3 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set second, third one to withdrawn
		game.getPlayers().get(1).setWithdraw(true);
		game.getPlayers().get(2).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(0), winner);
	}
	
	@Test
	public void testFindWinner6() {
		//add 3 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first, third one to withdrawn
		game.getPlayers().get(2).setWithdraw(true);
		game.getPlayers().get(0).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(1), winner);
	}
	
	@Test
	public void testFindWinner7() {
		//add 3 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first
		game.getPlayers().get(0).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(false, winnerExists);
	}
	
	@Test
	public void testFindWinner8() {
		//add 3 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set second
		game.getPlayers().get(1).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(false, winnerExists);
	}
	@Test
	public void testFindWinner9() {
		//add 4 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first, second, fourth
		game.getPlayers().get(0).setWithdraw(true);
		game.getPlayers().get(1).setWithdraw(true);
		game.getPlayers().get(3).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(2), winner);

	}

	@Test
	public void testFindWinner10() {
		//add 4 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set second, third, fourth
		game.getPlayers().get(3).setWithdraw(true);
		game.getPlayers().get(1).setWithdraw(true);
		game.getPlayers().get(2).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(0), winner);

	}
	@Test
	public void testFindWinner11() {
		//add 4 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first, second, third
		game.getPlayers().get(0).setWithdraw(true);
		game.getPlayers().get(1).setWithdraw(true);
		game.getPlayers().get(2).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(true, winnerExists);
		assertEquals(game.getPlayers().get(3), winner);

	}
	
	@Test
	public void testFindWinner12() {
		//add 4 players
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		game.getPlayers().add(new Player());
		
		//set first
		game.getPlayers().get(0).setWithdraw(true);
		
		boolean winnerExists = game.isWinner(game.getPlayers());
		Player winner = game.findWinner(game.getPlayers());
		
		assertEquals(false, winnerExists);

	}
}
