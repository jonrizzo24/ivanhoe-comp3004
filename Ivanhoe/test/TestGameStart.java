import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;

public class TestGameStart {
	
	Game game;
	

	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGameStart");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGameStart");
		game = new Game();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGameStart");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGameStart");
    }
	
	@Test
	public void twoPlayer() {
		for(int i = 0; i < 2; i++){
			Player p = new Player();
			game.addPlayer(p);
		}
		game.startGame(); //function that sets the objective variable to find winner
		assertEquals(2, game.getNumPlayers());
		assertEquals(5, game.GetObjective());
	}
	
	@Test
	public void threePlayer() {
		for(int i = 0; i < 3; i++){
			Player p = new Player();
			game.addPlayer(p);
		}
		game.startGame(); //function that sets the objective variable to find winner
		assertEquals(3, game.getNumPlayers());
		assertEquals(5, game.GetObjective());
	}
	
	@Test
	public void fourPlayer() {
		for(int i = 0; i < 4; i++){
			Player p = new Player();
			game.addPlayer(p);
		}
		game.startGame(); //function that sets the objective variable to find winner
		assertEquals(4, game.getNumPlayers());
		assertEquals(4, game.GetObjective());
	}
	
	@Test
	public void fivePlayer() {
		for(int i = 0; i < 5; i++){
			Player p = new Player();
			game.addPlayer(p);
		}
		game.startGame(); //function that sets the objective variable to find winner
		assertEquals(5, game.getNumPlayers());
		assertEquals(4, game.GetObjective());
	}

}
